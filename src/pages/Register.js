import { Fragment, useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
// Import stylish alert
import Swal from 'sweetalert2';
// same as <input> sa HTML ang <Form.Control>	
export default function Register () {
	
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// Hindi tayo makakapagtype using this variables because of the useState / initial value

	// gagamitan ng useState() sa Button
	const [isActive, setIsActive] = useState(false);

	// Check if the value are successfully not binded or changed
	console.log(email)
	console.log(password1)
	console.log(password2)

	// useEffect() for button fo rendering purposes
	useEffect(()=>{
		// Validation to enable submit button whwn all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== ' ') && (password1 === password2)){ // verification statement
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password1, password2]) // getter / initial value ang ilalagay sa array
										// controls the arrow function
										// for rendering purposes only lang. walang mababago
	
	// for the alert and after data submission
	function registerUser(e){ // e or parameter e is required when making an event listener / virtual DOM
		e.preventDefault();
		setEmail('')
		setPassword1('')
		setPassword2('') // this will remove all the input data after hitting the alert
		/*alert('Thank you for registering!')*/
		/*Swal.fire('Thank you for registering!')*/
		Swal.fire({
			title: 'YaaaAAaaAAAaaayyYYyy huehue!',
			icon: 'success',
			text: 'You have successfully registered!'
		})
	}

	// Two-way binding
		// The values in the fields of the form is bound to the getter of the state and the event is bound to the setter.
		// This is called two way binding
			// The data we change in the view has updated the state
			// The data in the state has updated the view

	return (
		<Fragment>
			<h1>Register</h1>
			<Form onSubmit={(e) => registerUser(e)}> {/*para hindi magblank ang input fields upon submit button*/}
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control 
						type="email"
						placeholder="Enter your Email"
						value={email} // I can't type dahil "" ang initial value. I need an onChange() event.
						onChange={e => setEmail(e.target.value)} // the e.target.value property allows us to gain access to the input field's current value to be used when submitting form data
						// we use this to apply two way binding to break the default value of the state hook. Para makapagtype sa input fields
						required
					/> 
					<Form.Text className="text-muted">
						We'll naver share your email with anyone else!
					</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter your Password"
						value={password1} // if no onChange, binding ang value or locked sa ""
						onChange={e => setPassword1(e.target.value)} // This is called two-way binding
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Verify Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify your Password"
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>
				{isActive ?
				<Button variant="success" type="submit" id="submitBtn">Submit</Button>
						  :
				<Button variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
				}
			</Form>
		</Fragment>
	)
}