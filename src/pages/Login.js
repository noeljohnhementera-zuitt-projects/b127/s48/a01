import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

//React Context
import UserContext from '../UserContext'

export default function Login () {
	// useContext is a React hook used to unwrap our context. It will return the data passed as values by a provider(UserContext.Provider component in App.js)
	const { user, setUser } = useContext(UserContext)

	// useState() to Input Fields
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('') 

	// useState() Login Button
	const [isAuthenticated, setIsAuthenticated] = useState(false)

	// useEffect() to enable Login Button
	useEffect(() =>{
		if(email !== '' && password !== ''){
			setIsAuthenticated(true)
		}else{
			setIsAuthenticated(false)
		}
	}, [email, password])

	// Login alert and clear characters in the input field
	const clearInputData = (e) =>{
		e.preventDefault();
		setEmail('')
		setPassword('')

		Swal.fire({
			title: 'Authentication Successful!',
			icon: 'success',
			text: 'You have successfully logged in!'
		})

		// React Context
		// local storage allows us to save data within our browser as strings
		// The setItem() method of the Storage Interface, when passed a key name and value, will add that key to the given storage ocjext, or update the key's value if it already exists
		// setItem is used to store data in the localStorage as a string
		// setItem('key', value)
		localStorage.setItem('email', email);
		setUser ( {email: email} )
	}

	return (
		<Fragment>
			<h1>Login</h1>
			<Form onSubmit={(e)=> clearInputData(e)}>
				<Form.Group>
					<Form.Label>Email Address:</Form.Label>
					<Form.Control
						type="email"
						value={email}
						onChange={e => setEmail(e.target.value)}
						placeholder="Enter Your Email Address"
						required
					/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control
						type="password"
						value={password}
						onChange={e => setPassword(e.target.value)}
						placeholder="Enter Your Password"
						required
					/>
				</Form.Group>
				{isAuthenticated ?
				<Button variant="success" type="submit">Login</Button>
								 :
				<Button variant="danger" type="submit" disabled>Login</Button>
				}
			</Form>
		</Fragment>
	)
}